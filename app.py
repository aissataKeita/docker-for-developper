"""
Simple example to run a python app in docker
"""

import os
import socket

from flask import Flask
from redis import Redis, RedisError
from importlib.metadata import version, requires, files

# Connect to Redis
redis = Redis(host="redis_db", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route("/")
def hello():
    """
    Simple hello world
    """
    try:
        visits = redis.incr("counter")
    except RedisError:
        visits = "<i>cannot connect to Redis, counter disabled</i>"

    flask_version = version('flask')
    html = "<h3>Hello {name}!</h3>" \
           "<b>Hostname:</b> {hostname}<br/>" \
           "<b>Visits:</b> {visits}<br/>" \
           "Powered by Flask {flask_version}"
    return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits, flask_version=flask_version)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8080)
